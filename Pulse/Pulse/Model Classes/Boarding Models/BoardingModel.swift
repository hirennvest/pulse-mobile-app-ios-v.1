//
//  BoardingModel.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-14.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

struct BoardingModel {
    
    //MARK: - iVars
    //Keep it Immutable! don't get Dirty :P
    private let stories: OnBoardingRootClass? = {
        do {
            return try LoaderFile.loadMockFile(named: "Caroseul.json", bundle: .main)
        }catch let e as LoaderError {
            debugPrint(e.description)
        }catch{
            debugPrint("could not read Mock json file :(")
        }
        return nil
    }()
    
    //MARK: - Public functions
    public func getStories() -> OnBoardingRootClass? {
        return stories
    }
    
    public mutating func numberOfItems(_ section: Int) -> Int {
        return (stories?.count)!
    }
    
    public func cellForItemAt(indexPath:IndexPath) -> OnBoardingBoarding? {
        return stories?.boarding![indexPath.row]
    }
    
}

enum LoaderError: Error, CustomStringConvertible {
    case invalidFileName(String)
    case invalidFileURL(URL)
    case invalidJSON(String)
    var description: String {
        switch self {
        case .invalidFileName(let name): return "\(name) FileName is incorrect"
        case .invalidFileURL(let url): return "\(url) FilePath is incorrect"
        case .invalidJSON(let name): return "\(name) has Invalid JSON"
        }
    }
}

struct LoaderFile {
    //@Note:XCTestCase will go for differnt set of bundle
    static func loadMockFile(named fileName:String,bundle:Bundle = .main) throws -> OnBoardingRootClass {
        guard let url = bundle.url(forResource: fileName, withExtension: nil) else {throw LoaderError.invalidFileName(fileName)}
        do {
            let data = try Data.init(contentsOf: url)
            if let _ = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? [String:Any] {
                let stories = try JSONDecoder().decode(OnBoardingRootClass.self, from: data)
                return stories
            }else {
                throw LoaderError.invalidFileURL(url)
            }
        }catch {
            throw LoaderError.invalidJSON(fileName)
        }
    }
    static func loadAPIResponse(response: [String: Any]) throws -> OnBoardingRootClass {
        let data = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
        do {
            let stories = try JSONDecoder().decode(OnBoardingRootClass.self, from: data)
            return stories
        } catch {
            throw LoaderError.invalidJSON("Input Response")
        }
    }
}
