//
//	OnBoardingRootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct OnBoardingRootClass : Codable {

	let boarding : [OnBoardingBoarding]?
	let count : Int?


	enum CodingKeys: String, CodingKey {
		case boarding = "boarding"
		case count = "count"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		boarding = try values.decodeIfPresent([OnBoardingBoarding].self, forKey: .boarding)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
	}


}