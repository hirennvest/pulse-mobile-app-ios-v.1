//
//	OnBoardingBoarding.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct OnBoardingBoarding : Codable {

	let uSD : String?
	let amount : String?
	let id : String?
	let name : String?
	let notes : String?
	let picture : String?


	enum CodingKeys: String, CodingKey {
		case uSD = "USD"
		case amount = "amount"
		case id = "id"
		case name = "name"
		case notes = "notes"
		case picture = "picture"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		uSD = try values.decodeIfPresent(String.self, forKey: .uSD)
		amount = try values.decodeIfPresent(String.self, forKey: .amount)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		notes = try values.decodeIfPresent(String.self, forKey: .notes)
		picture = try values.decodeIfPresent(String.self, forKey: .picture)
	}


}