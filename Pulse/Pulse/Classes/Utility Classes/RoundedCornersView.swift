//
//  RoundedCornersView.swift
//  PinterestLayout
//
//  Created by Khrystyna Shevchuk on 7/4/17.
//  Copyright © 2017 MagicLab. All rights reserved.
//

import UIKit


@IBDesignable
class RoundedCornersView: UIButton {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable
    public var clipbound: Bool = true {
        didSet {
            self.clipsToBounds = self.clipbound
        }
    }
    
    @IBInspectable
    public var borderwidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = self.borderwidth
        }
    }
    
    @IBInspectable
    public var bordercolor: UIColor = .clear {
        didSet {
            self.layer.borderColor = self.bordercolor.cgColor
        }
    }
    
    @IBInspectable
    public var CircleRadious: Bool = false {
        didSet {
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


@IBDesignable
class RoundedCornersImage: UIImageView {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable
    public var clipbound: Bool = true {
        didSet {
            self.clipsToBounds = self.clipbound
        }
    }
    
    @IBInspectable
    public var borderwidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = self.borderwidth
        }
    }
    
    @IBInspectable
    public var bordercolor: UIColor = .clear {
        didSet {
            self.layer.borderColor = self.bordercolor.cgColor
        }
    }
    
    @IBInspectable
    public var CircleRadious: Bool = false {
        didSet {
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

@IBDesignable
class RoundedCornersButton: UIButton {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable
    public var clipbound: Bool = true {
        didSet {
            self.clipsToBounds = self.clipbound
        }
    }
    
    @IBInspectable
    public var borderwidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = self.borderwidth
        }
    }
    
    @IBInspectable
    public var bordercolor: UIColor = .clear {
        didSet {
            self.layer.borderColor = self.bordercolor.cgColor
        }
    }
    
    @IBInspectable
    public var CircleRadious: Bool = false {
        didSet {
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
