//
//  AppDelegate.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVGKit
import CRNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let window: UIWindow = {
        let w = UIWindow()
        w.backgroundColor = .white
        w.makeKeyAndVisible()
        return w
    }()
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

//        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.enableAutoToolbar = true
//        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        
        #if Dev_DEBUG || Dev_RELEASE
            print("Dev")
        #elseif UAT_DEBUG || UAT_RELEASE
            print("UAT")
        #elseif QA_DEBUG || QA_RELEASE
            print("QA")
        #else
            print("Prod")
        #endif
    
        self.GetinitialView()
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
            return self.orientationLock
    }

}

extension AppDelegate {
    
    @objc public func GetinitialView() {
        let userinfo = UserInfoData.shared.GetUserInfodata()
        if userinfo == nil || userinfo!.accessToken.count == 0 {
//            self.SetLoginVC()
            self.OnBoarding()
        }
        else {
            self.SetESHomeTabVC()
        }
    }
    
    @objc public func SetESHomeTabVC() {
        let vc = loadViewController("Main", "MainTabBarVC")
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
    @objc public func OnBoarding() {
        let vc = OnBoardingVC.init(nibName: "OnBoardingVC", bundle: nil)
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
    @objc public func SetLoginVC() {
        let vc = loadViewController("Main", "LoginVC")
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
}
