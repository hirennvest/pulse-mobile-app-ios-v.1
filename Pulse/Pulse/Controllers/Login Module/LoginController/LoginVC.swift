//
//  LoginVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import IQKeyboardManagerSwift
import CRNotifications


class LoginVC: BaseVC {
    
    //    MARK:- IBOutlet Define
    
    @IBOutlet weak var bgimage: UIImageView!
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var TextFieldsView: UIView!
    @IBOutlet weak var UsernameView: UIView!
    @IBOutlet weak var TXTUsername: UITextField!
    
    @IBOutlet weak var Passwordview: UIView!
    @IBOutlet weak var TXTPassword: UITextField!
    @IBOutlet weak var LoginBTN: UIButton!
    
    @IBOutlet weak var ForgotPassBTN: UIButton!
    
    @IBOutlet weak var ConnectView: UIView!
    @IBOutlet weak var ConnectBTN: UIButton!
    
    @IBOutlet weak var SignupBTN: UIButton!
    
    //    MARK:- Variable Defines
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.setNeedsStatusBarAppearanceUpdate()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.TextFieldsView
        
        self.LoginBTN.isHidden = true
        
        self.TXTPassword.delegate = self
        self.TXTUsername.delegate = self
        
        self.UsernameView.clipsToBounds = true
        self.UsernameView.layer.cornerRadius = 10
        
        self.Passwordview.clipsToBounds = true
        self.Passwordview.layer.cornerRadius = 10
        
        self.ConnectView.clipsToBounds = true
        self.ConnectView.layer.cornerRadius = 10
        self.ConnectBTN.setTitle("CONNECT".customStringFormatting(), for: .normal)
        
        self.SignupBTN.setTitle("New to Pulse".localized(), for: .normal)
        self.ForgotPassBTN.setTitle("Reset Credentials".localized(), for: .normal)
        
        
        self.TXTUsername.attributedPlaceholder =
            NSAttributedString(string: "ID".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        self.TXTPassword.attributedPlaceholder =
            NSAttributedString(string: "PASSWORD".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.setupTextfield()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func setupTextfield() {
        
//        self.TXTUsername.text = "mani@nvestbank.com"
//        self.TXTPassword.text = "M@cbeth98"
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.TXTUsername.inputAccessoryView = toolbar
        self.TXTPassword.inputAccessoryView = toolbar
    }
    
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
        self.TXTUsername.resignFirstResponder()
        self.TXTPassword.resignFirstResponder()
        self.LoginBTN.isHidden = true
        self.bgimage.isHidden = false
    }
    
    @IBAction func ForgotPassTapped(_ sender: Any) {
        let vc = ForgotPasswordVC.init(nibName: "ForgotPasswordVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ConnectTapped(_ sender: Any) {
        
        if AppUtillity.shared.validateEmail(enteredEmail: self.TXTUsername.text!.removeWhiteSpace()) {
            self.UsernameView.layer.borderColor = UIColor.clear.cgColor
            self.UsernameView.layer.borderWidth = 0
        }
        else {
            self.UsernameView.layer.borderColor = UIColor.red.cgColor
            self.UsernameView.layer.borderWidth = 0.5
            return
        }
        
        if self.TXTPassword.text!.count >= 8 {
            self.Passwordview.layer.borderColor = UIColor.clear.cgColor
            self.Passwordview.layer.borderWidth = 0
        }
        else {
            self.Passwordview.layer.borderColor = UIColor.red.cgColor
            self.Passwordview.layer.borderWidth = 0.5
            return
        }
        
        let paramDict = LoginParamDict.init(ids: self.TXTUsername.text!.removeWhiteSpace(), password: self.TXTPassword.text!.removeWhiteSpace())
        
        LottieHUD.shared.showHUD()
        NetworkingRequests.shared.NetworkPostrequestJSON(PlistKey.LoginApi, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
            if statuscode {
                LottieHUD.shared.stopHUD()
                let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                UserInfoData.shared.SaveUserInfodata(info: userinfo)
                App?.SetESHomeTabVC()
            }
            else {
                LottieHUD.shared.stopHUD()
                let message = responseObject["message"] as AnyObject
                CRNotifications.showNotification(type: CRNotifications.info, title: "INFO!".localized(), message: (message as? String)!, dismissDelay: 3, completion: {
                    
                })
            }
        }) { (msg, code) in
            LottieHUD.shared.stopHUD()
            CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!".localized(), message: msg, dismissDelay: 3, completion: {
                
            })
        }
    }
    
    @IBAction func SignupTapped(_ sender: Any) {
        let vc = PickGXidVC.init(nibName: "PickGXidVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Textfield Delegates
extension LoginVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        self.bgimage.isHidden = true
        if textField == TXTUsername {
            self.TXTUsername.becomeFirstResponder()
            self.LoginBTN.isHidden = true
            self.TXTUsername.returnKeyType = .next
        }
        else {
            self.TXTPassword.becomeFirstResponder()
            self.LoginBTN.isHidden = false
            self.TXTUsername.returnKeyType = .done
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == TXTUsername {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let count = text.count
            if string != "" {
                if count % 2 == 0{
                    textField.text?.insert(" ", at: String.Index.init(encodedOffset: count - 1))
                }
            }
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            print(newString)
        }
        return true
    }
    
}
