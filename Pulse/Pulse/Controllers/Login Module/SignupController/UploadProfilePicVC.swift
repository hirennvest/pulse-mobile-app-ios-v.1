//
//  UploadProfilePicVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-31.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import CRNotifications


class UploadProfilePicVC: BaseVC {
    
    //    MARK:- IBOutlet Define
    
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var ProfileBTN: UIButton!
    @IBOutlet weak var Profile_Width: NSLayoutConstraint!
    @IBOutlet weak var Profile_Height: NSLayoutConstraint!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    @IBOutlet weak var ConnectBTN: UIButton!
    
    //    MARK:- Variable Defines
    
    public var GXid: String!
    public var Email: String!
    public var Passwrod: String!
    public var ConfirmPasswrod: String!
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.MessageLBL.text = "UPLOAD \nPROFILE PIC".customStringFormatting()
        self.ProfileBTN.clipsToBounds = true
        self.Profile_Width.constant = 100
        self.Profile_Height.constant = 100
        self.ProfileBTN.layer.cornerRadius = self.Profile_Height.constant / 2
        self.ProfileBTN.layer.borderColor = UIColor.white.cgColor
        self.ProfileBTN.layer.borderWidth = 1
        
        self.ConnectBTN.setTitle("CONNECT".customStringFormatting(), for: .normal)
    }
    
    //    MARK:- APicalling
    
    func SignupwithoutProfile() {
        let paramDict = SignupParamDict.init(username: self.GXid, email: self.Email, password: self.Passwrod, ref_affiliate: "1", account_type: "personal", signedup_app: "GX")
        
        LottieHUD.shared.showHUD()
        
        NetworkingRequests.shared.NetworkPostrequestJSON(PlistKey.SignupApi, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
            let message = responseObject["message"] as AnyObject
            if statuscode {
                LottieHUD.shared.stopHUD()
                let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                UserInfoData.shared.SaveUserInfodata(info: userinfo)
                App?.SetESHomeTabVC()
            }
            else {
                LottieHUD.shared.stopHUD()
                CRNotifications.showNotification(type: CRNotifications.info, title: "INFO!".localized(), message: message as! String, dismissDelay: 3, completion: {
                })
            }
        }) { (msg, code) in
            LottieHUD.shared.stopHUD()
            CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!".localized(), message: msg, dismissDelay: 3, completion: {})
        }
    }
    
    func SignupwithProfile() {
        
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ProfileBTN(_ sender: Any) {
        AttachmentHandler.shared.ShowSelectedAttachOption(vc: self, constant: [.camera, .photoLibrary])
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.Profile_Width.constant = 250
            self.Profile_Height.constant = 250
            self.MessageLBL.text = "@ " + self.GXid
            self.ProfileBTN.layer.cornerRadius = self.Profile_Height.constant / 2
            self.ProfileBTN.setImage(image, for: .normal)
        }
    }
    
    @IBAction func ConnectBTN(_ sender: Any) {
        self.SignupwithoutProfile()
    }
    
}
