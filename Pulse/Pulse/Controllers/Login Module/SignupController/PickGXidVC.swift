//
//  PickGXidVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-31.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class PickGXidVC: BaseVC {

    //    MARK:- IBOutlet Define
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var IDview: UIView!
    @IBOutlet weak var GXIDview: UIView!
    @IBOutlet weak var TXTGXID: UITextField!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var ClearBTN: UIButton!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    //    MARK:- Variable Defines
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.submain
        
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        
        self.TXTGXID.delegate = self
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.GXIDview.clipsToBounds = true
        self.GXIDview.layer.cornerRadius = 10
        
        self.TXTGXID.attributedPlaceholder =
            NSAttributedString(string: "PICK GX - ID".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.MessageLBL.text = "AGX - ID WILL ALLOW YOU TO ACCESS ALL GXAPPS".customStringFormatting()
        
        self.setupTextfield()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func setupTextfield() {
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.TXTGXID.inputAccessoryView = toolbar
        
    }
    
    func IDNotAvailable() {
        self.MessageLBL.text = "ID ALREADY IN USE".customStringFormatting()
        self.ClearBTN.isHidden = false
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
        self.NextBTN.isHidden = true
    }
    
    func IDAvailable() {
        self.MessageLBL.text = "ID AVAILABLE".customStringFormatting()
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = false
    }
    
//    MARK:- API Calling
    
    func SignupWithGXid(idvalue: String) {
        let paramDict = SignupParamDict.init(username: idvalue.removeWhiteSpace(), email: "".removeWhiteSpace(), password: "".removeWhiteSpace(), ref_affiliate: "1", account_type: "personal", signedup_app: "GX")
        
        NetworkingRequests.shared.NetworkPostrequestJSON(PlistKey.SignupApi, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
            let apistatus: Bool = responseObject["status"] as! Bool
            if statuscode && apistatus {
                self.IDAvailable()
            }
            else {
                self.IDNotAvailable()
            }
        }) { (msg, code) in
            self.IDNotAvailable()
        }
    }
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClearTapped(_ sender: Any) {
        self.TXTGXID.text = ""
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        let vc = EmailAddressVC.init(nibName: "EmailAddressVC", bundle: nil)
        vc.GXid = self.TXTGXID.text
        vc.Email = ""
        vc.Passwrod = ""
        vc.ConfirmPasswrod = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK:- Textfield Delegates
extension PickGXidVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
        }
        if textField == TXTGXID {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let count = text.count
            if string != "" {
                if count % 2 == 0{
                    textField.text?.insert(" ", at: String.Index.init(encodedOffset: count - 1))
                }
            }
        }
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        print(newString)
        
        if newString.length >= 3 {
            self.SignupWithGXid(idvalue: (newString as String).removeWhiteSpace())
        }
        else {
            self.IDNotAvailable()
        }
        
//        if (newString as String).removeWhiteSpace() == "hirenjoshi" {
//            self.IDAvailable()
//        }
//        else {
//            self.IDNotAvailable()
//        }
        
        return true
    }
    
}

