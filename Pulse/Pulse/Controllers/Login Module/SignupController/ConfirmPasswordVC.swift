//
//  ConfirmPasswordVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-31.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class ConfirmPasswordVC: BaseVC {
    
    //    MARK:- IBOutlet Define
    
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var Passview: UIView!
    @IBOutlet weak var Passwordview: UIView!
    @IBOutlet weak var TXTPassword: UITextField!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var ClearBTN: UIButton!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    //    MARK:- Variable Defines
    
    public var GXid: String!
    public var Email: String!
    public var Passwrod: String!
    public var ConfirmPasswrod: String!
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.submain
        
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        
        self.TXTPassword.delegate = self
        
        self.Passwordview.clipsToBounds = true
        self.Passwordview.layer.cornerRadius = 10
        
        self.TXTPassword.attributedPlaceholder =
            NSAttributedString(string: "CONFIRM PASSWORD".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.MessageLBL.text = ""
        
        self.setupTextfield()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func setupTextfield() {
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.TXTPassword.inputAccessoryView = toolbar
        
    }
    
    func Checkpassword(entervalue: String) {
        if self.Passwrod == entervalue {
            self.MessageLBL.text = "PASSWORD MATCH".customStringFormatting()
            self.BackBTN.backgroundColor = .white
            self.BackBTN.tintColor = .black
            self.ClearBTN.isHidden = true
            self.NextBTN.isHidden = false
        }
        else {
            self.MessageLBL.text = "PASSWORD DON`T MATCH".customStringFormatting()
            self.BackBTN.backgroundColor = AppDarkColor
            self.BackBTN.tintColor = .white
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
        }
    }
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClearTapped(_ sender: Any) {
        self.TXTPassword.text = ""
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        let vc = UploadProfilePicVC.init(nibName: "UploadProfilePicVC", bundle: nil)
        vc.GXid = self.GXid
        vc.Email = self.Email
        vc.Passwrod = self.Passwrod
        vc.ConfirmPasswrod = self.TXTPassword.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Textfield Delegates
extension ConfirmPasswordVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            self.ClearBTN.isHidden = false
            self.BackBTN.backgroundColor = AppDarkColor
            self.BackBTN.tintColor = .white
            self.NextBTN.isHidden = true
        }
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        
        self.Checkpassword(entervalue: newString as String)
        return true
    }
    
}
