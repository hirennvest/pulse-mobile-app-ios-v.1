//
//  EmailAddressVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-31.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class EmailAddressVC: BaseVC {
    
    //    MARK:- IBOutlet Define
    
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var Emailview: UIView!
    @IBOutlet weak var Emailaddview: UIView!
    @IBOutlet weak var TXTEmail: UITextField!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var ClearBTN: UIButton!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    //    MARK:- Variable Defines
    
    public var GXid: String!
    public var Email: String!
    public var Passwrod: String!
    public var ConfirmPasswrod: String!
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.submain
        
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        
        self.TXTEmail.delegate = self
        
        self.Emailaddview.clipsToBounds = true
        self.Emailaddview.layer.cornerRadius = 10
        
        self.TXTEmail.attributedPlaceholder =
            NSAttributedString(string: "ENTER EMAIL".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.MessageLBL.text = ""
        
        self.setupTextfield()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func setupTextfield() {
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.TXTEmail.inputAccessoryView = toolbar
        
    }
    
    func IDNotAvailable() {
        self.MessageLBL.text = "EMAIL ALREADY IN USE".customStringFormatting()
        self.ClearBTN.isHidden = false
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
        self.NextBTN.isHidden = true
    }
    
    func IDAvailable() {
        self.MessageLBL.text = "GOOD TO GO".customStringFormatting()
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = false
    }
    
    //    MARK:- API Calling

    func SignupWithEmail(idvalue: String) {
        let paramDict = SignupParamDict.init(username: self.GXid, email: idvalue, password: "", ref_affiliate: "1", account_type: "personal", signedup_app: "GX")
        
        LottieHUD.shared.showHUD()
        NetworkingRequests.shared.NetworkPostrequestJSON(PlistKey.SignupApi, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
            let apistatus: Bool = responseObject["status"] as! Bool
            if statuscode && apistatus {
                LottieHUD.shared.stopHUD()
                self.IDAvailable()
            }
            else {
                LottieHUD.shared.stopHUD()
                self.IDNotAvailable()
            }
        }) { (msg, code) in
            LottieHUD.shared.stopHUD()
            self.IDNotAvailable()
        }
    }
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClearTapped(_ sender: Any) {
        self.TXTEmail.text = ""
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = true
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        let vc = CreatePasswordVC.init(nibName: "CreatePasswordVC", bundle: nil)
        vc.GXid = self.GXid
        vc.Email = self.TXTEmail.text
        vc.Passwrod = ""
        vc.ConfirmPasswrod = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Textfield Delegates
extension EmailAddressVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
        }
        if textField == TXTEmail {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let count = text.count
            if string != "" {
                if count % 2 == 0{
                    textField.text?.insert(" ", at: String.Index.init(encodedOffset: count - 1))
                }
            }
        }
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        print(newString)
        
        if AppUtillity.shared.validateEmail(enteredEmail: (newString as String).removeWhiteSpace()) {
            self.SignupWithEmail(idvalue: newString as String)
        }
        else {
            self.IDNotAvailable()
        }
        
//        if (newString as String).removeWhiteSpace() == "hiren@nvestbank.com" {
//            if AppUtillity.shared.validateEmail(enteredEmail: newString as String) {
//                self.IDAvailable()
//            }
//            else {
//                self.IDNotAvailable()
//            }
//        }
//        else {
//            self.IDNotAvailable()
//        }
        
        return true
    }
    
}
