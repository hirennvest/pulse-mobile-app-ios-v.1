//
//  CreatePasswordVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-31.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class CreatePasswordVC: BaseVC {
    
    //    MARK:- IBOutlet Define
    
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var Passview: UIView!
    @IBOutlet weak var Passwordview: UIView!
    @IBOutlet weak var TXTPassword: UITextField!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var ClearBTN: UIButton!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    //    MARK:- Variable Defines
    let char = "8 CHARACTERS".customStringFormatting()
    let upper = "\n1 UPPER CASE".customStringFormatting()
    let num = "\n1 NUMBER".customStringFormatting()
    let special = "\n1 SPECIAL CHARACTER".customStringFormatting()
    
    public var GXid: String!
    public var Email: String!
    public var Passwrod: String!
    public var ConfirmPasswrod: String!
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.submain
        
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        
        self.TXTPassword.delegate = self
        
        self.Passwordview.clipsToBounds = true
        self.Passwordview.layer.cornerRadius = 10
        
        self.TXTPassword.attributedPlaceholder =
            NSAttributedString(string: "ENTER PASSWORD".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        let mainstring: String = char + upper + num + special
        let myMutableString = mainstring.Attributestring(attribute: [(char, UIFont.systemFont(ofSize: 15.0), UIColor.white), (upper, UIFont.systemFont(ofSize: 15.0), UIColor.white), (num, UIFont.systemFont(ofSize: 15.0), UIColor.white), (special, UIFont.systemFont(ofSize: 15.0), UIColor.white)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [char, upper, num, special])))
        self.MessageLBL.attributedText = myMutableString
        
        self.setupTextfield()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func setupTextfield() {
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.TXTPassword.inputAccessoryView = toolbar
        
    }
    
    func IDNotAvailable() {
        self.ClearBTN.isHidden = false
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
        self.NextBTN.isHidden = true
    }
    
    func IDAvailable() {
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = false
    }
    
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClearTapped(_ sender: Any) {
        self.TXTPassword.text = ""
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        let vc = ConfirmPasswordVC.init(nibName: "ConfirmPasswordVC", bundle: nil)
        vc.GXid = self.GXid
        vc.Email = self.Email
        vc.Passwrod = self.TXTPassword.text
        vc.ConfirmPasswrod = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Textfield Delegates
extension CreatePasswordVC: UITextFieldDelegate {
    
    func IsContainCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    func IsContainUppercase(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    func IsContainDigit(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "0123456789")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    func IsContainSpecialCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if textvalue.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
     
        // at least one uppercase,
        // at least one digit
        // at least one special character
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        let mainstring: String = char + upper + num + special
        let myMutableString = mainstring.Attributestring(attribute: [
            (char, UIFont.systemFont(ofSize: 15.0), self.TXTPassword.text!.count >= 8 ? UIColor.white : UIColor.lightGray),
            (upper, UIFont.systemFont(ofSize: 15.0), self.IsContainUppercase(textvalue: textField.text!) ? UIColor.white : UIColor.lightGray),
            (num, UIFont.systemFont(ofSize: 15.0), self.IsContainDigit(textvalue: textField.text!) ? UIColor.white : UIColor.lightGray),
            (special, UIFont.systemFont(ofSize: 15.0), self.IsContainSpecialCharacter(textvalue: textField.text!)  ? UIColor.white : UIColor.lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [char, upper, num, special])))
        self.MessageLBL.attributedText = myMutableString
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
        }
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        
        let mainstring: String = char + upper + num + special
        let myMutableString = mainstring.Attributestring(attribute: [
            (char, UIFont.systemFont(ofSize: 15.0), self.TXTPassword.text!.count >= 8 ? UIColor.white : UIColor.lightGray),
            (upper, UIFont.systemFont(ofSize: 15.0), self.IsContainUppercase(textvalue: newString as String) ? UIColor.white : UIColor.lightGray),
            (num, UIFont.systemFont(ofSize: 15.0), self.IsContainDigit(textvalue: newString as String) ? UIColor.white : UIColor.lightGray),
            (special, UIFont.systemFont(ofSize: 15.0), self.IsContainSpecialCharacter(textvalue: newString as String)  ? UIColor.white : UIColor.lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [char, upper, num, special])))
        self.MessageLBL.attributedText = myMutableString
        
        if newString.length >= 8 && self.IsContainUppercase(textvalue: newString as String) && self.IsContainDigit(textvalue: newString as String) && self.IsContainSpecialCharacter(textvalue: newString as String){
            self.ClearBTN.isHidden = true
            self.BackBTN.backgroundColor = .white
            self.BackBTN.tintColor = .black
            self.NextBTN.isHidden = false
        }
        
        return true
    }
    
}

