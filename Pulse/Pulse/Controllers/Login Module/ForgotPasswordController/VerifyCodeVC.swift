//
//  VerifyCodeVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-01.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import SVPinView

class VerifyCodeVC: BaseVC {
    
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var Codesview: UIView!
    @IBOutlet weak var Codeview: UIView!
    @IBOutlet weak var TXTCode: SVPinView!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var ClearBTN: UIButton!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    //    MARK:- Variable Defines
    
    public var Email: String!
    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.submain
        
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        
        self.Codeview.clipsToBounds = true
        self.Codeview.layer.cornerRadius = 10
        
        self.MessageLBL.text = ""
        
        self.configurePinView()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func IDNotAvailable() {
        self.MessageLBL.text = "VERIFY CODE ISN'T CORRECT".customStringFormatting()
        self.ClearBTN.isHidden = false
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
        self.NextBTN.isHidden = true
    }
    
    func IDAvailable() {
        self.MessageLBL.text = "GOOD TO GO".customStringFormatting()
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = false
    }
    
    func configurePinView() {
        
        self.TXTCode.pinLength = 6
        self.TXTCode.secureCharacter = "\u{25CF}"
        self.TXTCode.interSpace = 10
        self.TXTCode.textColor = UIColor.white
        self.TXTCode.borderLineColor = UIColor.white
        self.TXTCode.activeBorderLineColor = UIColor.white
        self.TXTCode.borderLineThickness = 1
        self.TXTCode.shouldSecureText = true
        self.TXTCode.allowsWhitespaces = false
////        TODO: Style None
//        self.TXTCode.style = .none
//        self.TXTCode.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
//        self.TXTCode.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
//        self.TXTCode.fieldCornerRadius = 15
//        self.TXTCode.activeFieldCornerRadius = 15
////        TODO: Style Box
//        self.TXTCode.activeBorderLineThickness = 4
//        self.TXTCode.fieldBackgroundColor = UIColor.clear
//        self.TXTCode.activeFieldBackgroundColor = UIColor.clear
//        self.TXTCode.fieldCornerRadius = 0
//        self.TXTCode.activeFieldCornerRadius = 0
//        self.TXTCode.style = .box
////        TODO: Style Underline
        self.TXTCode.activeBorderLineThickness = 4
        self.TXTCode.fieldBackgroundColor = UIColor.clear
        self.TXTCode.activeFieldBackgroundColor = UIColor.clear
        self.TXTCode.fieldCornerRadius = 0
        self.TXTCode.activeFieldCornerRadius = 0
        self.TXTCode.style = .underline
        
        self.TXTCode.placeholder = "******"
        self.TXTCode.shouldDismissKeyboardOnEmptyFirstField = false
        
        self.TXTCode.font = UIFont.systemFont(ofSize: 15)
        self.TXTCode.keyboardType = .phonePad
        self.TXTCode.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done".localized(), style: UIBarButtonItem.Style.done, target: self, action: #selector(DoneBTNAction))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
            self.IDNotAvailable()
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        self.TXTCode.didFinishCallback = didFinishEnteringPin(pin:)
        self.TXTCode.didstartentering = { pin in
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
            self.IDNotAvailable()
        }
        self.TXTCode.lessthenlength = { pin in
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
            self.IDNotAvailable()
        }
        self.TXTCode.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
            if pin.count == self.TXTCode.pinLength {
                self.ClearBTN.isHidden = true
                self.NextBTN.isHidden = false
                self.IDAvailable()
            }
            else {
                self.ClearBTN.isHidden = false
                self.NextBTN.isHidden = true
                self.IDNotAvailable()
            }
        }
    }
    
    func pastePin() {
        guard let pin = UIPasteboard.general.string else {
            return
        }
        self.TXTCode.pastePin(pin: pin)
    }
    
    func didFinishEnteringPin(pin:String) {
        self.IDAvailable()
    }
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClearTapped(_ sender: Any) {
        self.TXTCode.clearPin()
        self.ClearBTN.isHidden = false
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
        self.NextBTN.isHidden = true
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        let vc = ForgotCreatePasswordVC.init(nibName: "ForgotCreatePasswordVC", bundle: nil)
        vc.Email = self.Email
        vc.Verifycode = self.TXTCode.getPin()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
