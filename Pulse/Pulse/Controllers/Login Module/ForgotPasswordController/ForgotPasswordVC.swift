//
//  ForgotPasswordVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-03-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import CRNotifications

class ForgotPasswordVC: BaseVC {
    
    @IBOutlet weak var BackBTN: UIButton!
    
    @IBOutlet weak var submain: UIView!
    
    @IBOutlet weak var Emailview: UIView!
    @IBOutlet weak var Emailaddview: UIView!
    @IBOutlet weak var TXTEmail: UITextField!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var ClearBTN: UIButton!
    
    @IBOutlet weak var MessageLBL: UILabel!
    
    //    MARK:- Variable Defines

    
    //    MARK:- Life Cycle Define
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //    MARK:- User Define Methods
    
    func setupView() {
        KeyboardAvoiding.avoidingView = self.submain
        
        self.BackBTN.setImage(UIImage.init(named: "BackBTN"), for: .normal)
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        
        self.ClearBTN.isHidden = true
        self.NextBTN.isHidden = true
        
        self.TXTEmail.delegate = self
        
        self.Emailaddview.clipsToBounds = true
        self.Emailaddview.layer.cornerRadius = 10
        
        self.TXTEmail.attributedPlaceholder =
            NSAttributedString(string: "ENTER EMAIL".customStringFormatting(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        self.MessageLBL.text = ""
        
        self.setupTextfield()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(DoneBTNAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.submain.addGestureRecognizer(tap)
    }
    
    func setupTextfield() {
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.TXTEmail.inputAccessoryView = toolbar
        
    }
    
    func IDNotAvailable() {
        self.MessageLBL.text = "EMAIL ALREADY IN USE".customStringFormatting()
        self.ClearBTN.isHidden = false
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
        self.NextBTN.isHidden = true
    }
    
    func IDAvailable() {
        self.MessageLBL.text = "GOOD TO GO".customStringFormatting()
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = false
    }
    
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
    }
    
    @IBAction func BackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClearTapped(_ sender: Any) {
        self.TXTEmail.text = ""
        self.ClearBTN.isHidden = true
        self.BackBTN.backgroundColor = .white
        self.BackBTN.tintColor = .black
        self.NextBTN.isHidden = true
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        if AppUtillity.shared.validateEmail(enteredEmail: self.TXTEmail.text!) {
            self.Emailaddview.layer.borderColor = UIColor.clear.cgColor
            self.Emailaddview.layer.borderWidth = 0
        }
        else {
            self.Emailaddview.layer.borderColor = UIColor.red.cgColor
            self.Emailaddview.layer.borderWidth = 0.5
            return
        }
        
        let paramDict = ForgotRequestParamDict.init(email: self.TXTEmail.text!)
        
        LottieHUD.shared.showHUD()
        NetworkingRequests.shared.NetworkPostrequestJSON(PlistKey.ForgotpasswordRequestAPI, Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
            let message = responseObject["message"] as AnyObject
            if statuscode {
                LottieHUD.shared.stopHUD()
                CRNotifications.showNotification(type: CRNotifications.success, title: "SUCCESS!".localized(), message: message as! String, dismissDelay: 3, completion: {
                    let vc = VerifyCodeVC.init(nibName: "VerifyCodeVC", bundle: nil)
                    vc.Email = self.TXTEmail.text!
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            }
            else {
                LottieHUD.shared.stopHUD()
                CRNotifications.showNotification(type: CRNotifications.info, title: "INFO!".localized(), message: message as! String, dismissDelay: 3, completion: {
                    
                })
            }
        }) { (msg, code) in
            LottieHUD.shared.stopHUD()
            CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!".localized(), message: msg, dismissDelay: 3, completion: {
    
            })
        }
    
    }
    
}

//MARK:- Textfield Delegates
extension ForgotPasswordVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        self.BackBTN.backgroundColor = AppDarkColor
        self.BackBTN.tintColor = .white
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.count > 0 {
            self.ClearBTN.isHidden = false
            self.NextBTN.isHidden = true
        }
        
        if textField == TXTEmail {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let count = text.count
            if string != "" {
                if count % 2 == 0{
                    textField.text?.insert(" ", at: String.Index.init(encodedOffset: count - 1))
                }
            }
        }
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        print(newString)
        
        if AppUtillity.shared.validateEmail(enteredEmail: (newString as String).removeWhiteSpace()) {
            self.IDAvailable()
        }
        else {
            self.IDNotAvailable()
        }
        
        return true
    }
    
}
