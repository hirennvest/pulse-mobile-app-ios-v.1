//
//  BoardingCarouselCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-14.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import SDWebImage

class BoardingCarouselCell: UICollectionViewCell {
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Picture: CustomIMG!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Note: UILabel!
    @IBOutlet weak var Amount: UILabel!
    @IBOutlet weak var USD: UILabel!
    
    public var story: OnBoardingBoarding? {
        didSet {
            self.Name.text = story?.name
            if let picture = story?.picture {
//                self.Picture.sd_setImage(with: URL.init(string: picture), completed: nil)
                self.Picture.setImage(url: picture, style: .rounded, completion: nil)
            }
            self.Note.text = story?.notes
            self.Amount.text = story?.amount
            self.USD.text = story?.uSD
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
