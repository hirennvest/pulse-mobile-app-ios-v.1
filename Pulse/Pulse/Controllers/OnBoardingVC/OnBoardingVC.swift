//
//  OnBoardingVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-14.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class OnBoardingVC: BaseVC {

//    MARK:- IBOutlet
    
    @IBOutlet weak var Carousel: UICollectionView!
    @IBOutlet weak var ConnectBTN: UIButton!
    @IBOutlet weak var CreateBTN: UIButton!
    
    var begin = false
    var TR: Timer!
    private var viewModel: BoardingModel = BoardingModel()
    
//    MARK:- Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.ConnectBTN.setTitle("CONNECT".customStringFormatting(), for: .normal)
        self.CreateBTN.setTitle("CREATE".customStringFormatting(), for: .normal)
        
        self.Carousel.register(UINib.init(nibName: "BoardingCarouselCell", bundle: nil), forCellWithReuseIdentifier: "BoardingCarouselCell")
        self.Carousel.translatesAutoresizingMaskIntoConstraints = false
        
        self.Carousel.delegate = self
        self.Carousel.dataSource = self
        
        self.startTimer()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.TR.invalidate()
        self.TR = nil
        self.Carousel.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        self.Carousel.scrollRectToVisible(CGRect.zero, animated: true)
        self.begin = false
    }

//    MARK:- IBAction Methods

    @IBAction func ConnectTapped(_ sender: Any) {
        let vc = loadViewController("Main", "LoginVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func CreateTapped(_ sender: Any) {
        let vc = PickGXidVC.init(nibName: "PickGXidVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK: - Extension|UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension OnBoardingVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BoardingCarouselCell",for: indexPath) as? BoardingCarouselCell else { fatalError() }
        cell.contentView.clipsToBounds = true
        let story = viewModel.cellForItemAt(indexPath: indexPath)
        cell.story = story
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Screen_width, height: self.Carousel.frame.height)
    }
    
    @objc func scrollToNextCell(){

        //get cell size
        let cellSize = CGSize(width: Screen_width, height: self.Carousel.frame.height)

        //get current content Offset of the Collection view
        let contentOffset = self.Carousel.contentOffset;

            //scroll to next cell
        if begin == true
        {
            self.Carousel.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
            self.Carousel.scrollRectToVisible(CGRect.zero, animated: true)
            begin = false
        }
        else
        {
            let rect = CGRect.init(x: contentOffset.x + cellSize.width + 10, y: contentOffset.y, width: cellSize.width, height: cellSize.height)
            self.Carousel.scrollRectToVisible(rect, animated: true);
        }

    }
    
    func startTimer() {
        self.TR = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true);
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.Carousel.contentSize.width == self.Carousel.contentOffset.x + self.view.frame.width
        {
            begin = true
        }
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        // Called when manually setting contentOffset
        scrollViewDidEndDecelerating(scrollView)
    }
    
}
