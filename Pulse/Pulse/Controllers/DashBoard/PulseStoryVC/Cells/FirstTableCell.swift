//
//  FirstTableCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-02.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FirstTableCell: UITableViewCell {

    @IBOutlet weak var Subview: UIView!
    @IBOutlet weak var ProfileIMG: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var detaillbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Subview.clipsToBounds = true
        self.Subview.layer.cornerRadius = 10
        self.Subview.layer.borderColor = AppDarkColor.cgColor
        self.Subview.layer.borderWidth = 0.5
        
        self.ProfileIMG.clipsToBounds = true
        self.ProfileIMG.layer.cornerRadius = self.ProfileIMG.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
