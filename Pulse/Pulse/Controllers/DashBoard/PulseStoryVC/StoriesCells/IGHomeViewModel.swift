//
//  IGHomeViewModel.swift
//  InstagramStories
//
//  Created by  Boominadha Prakash on 01/11/17.
//  Copyright © 2017 DrawRect. All rights reserved.
//

import Foundation

struct IGHomeViewModel {
    
    //MARK: - iVars
    //Keep it Immutable! don't get Dirty :P
    var AddStoryBTN: Bool = false
    private let stories: IGStories? = {
        do {
            return try IGMockLoader.loadMockFile(named: "stories.json", bundle: .main)
        }catch let e as MockLoaderError {
            debugPrint(e.description)
        }catch{
            debugPrint("could not read Mock json file :(")
        }
        return nil
    }()
    
    //MARK: - Public functions
    public func getStories() -> IGStories? {
        return stories
    }
    
    public mutating func numberOfItemsInSection(_ section: Int, Addstory: Bool) -> Int {
        self.AddStoryBTN = Addstory
        if AddStoryBTN {
            if let count = stories?.count {
                return count + 1 // Add Story cell
            }
            return 1
        }
        else {
            if let count = stories?.count {
                return count
            }
            return 0
        }
    }
    
    public func cellForItemAt(indexPath:IndexPath) -> IGStory? {
        if AddStoryBTN {
            return stories?.stories[indexPath.row-1]
        }
        return stories?.stories[indexPath.row]
    }
    
}
