//
//  PulseStoryVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-01.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PulseStoryVC: BaseVC {

//    MARK:- IBOutlets
    @IBOutlet weak var StoriesCollection: UICollectionView!
    @IBOutlet weak var Stories_height: NSLayoutConstraint!
    @IBOutlet weak var FeedTBL: UITableView!
    
//    MARK:- Variable Define
    
    private var viewModel: IGHomeViewModel = IGHomeViewModel()
    
    lazy var layout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: 100, height: 100)
        return flowLayout
    }()
    
//    MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        
        self.StoriesCollection.register(IGStoryListCell.self, forCellWithReuseIdentifier: IGStoryListCell.reuseIdentifier)
        self.StoriesCollection.register(IGAddStoryCell.self, forCellWithReuseIdentifier: IGAddStoryCell.reuseIdentifier)
        self.StoriesCollection.translatesAutoresizingMaskIntoConstraints = false
        
        if viewModel.numberOfItemsInSection(0, Addstory: false) == 0 {
            self.Stories_height.constant = 0
        }
        else {
            self.Stories_height.constant = 130
            self.StoriesCollection.delegate = self
            self.StoriesCollection.dataSource = self
        }
        
        self.FeedTBL.register(UINib.init(nibName: "FirstTableCell", bundle: nil), forCellReuseIdentifier: "FirstTableCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        self.FeedTBL.tableFooterView = footer
        
        self.FeedTBL.delegate = self
        self.FeedTBL.dataSource = self
        self.FeedTBL.reloadData()
        
    }

//    MARK:- User Define Methods
    
    
//    MARK:- IBAction Methods

    @objc private func clearImageCache() {
        IGCache.shared.removeAllObjects()
        IGStories.removeAllVideoFilesFromCache()
        AlertView.showAlertMessage(withMsg: "Images & Videos are deleted from cache".localized(), withcontroller: self)
    }

}

//MARK: - Extension|UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension PulseStoryVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section, Addstory: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 && viewModel.AddStoryBTN {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IGAddStoryCell.reuseIdentifier, for: indexPath) as? IGAddStoryCell else { fatalError() }
            cell.userDetails = ("Your story","https://avatars2.githubusercontent.com/u/32802714?s=200&v=4")
            return cell
        }else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IGStoryListCell.reuseIdentifier,for: indexPath) as? IGStoryListCell else { fatalError() }
            let story = viewModel.cellForItemAt(indexPath: indexPath)
            cell.story = story
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && viewModel.AddStoryBTN {
            AlertView.showAlertMessage(withMsg: "Try to implement your own functionality for 'Your story'".localized(), withcontroller: self)
        }else {
            DispatchQueue.main.async {
                if let stories = self.viewModel.getStories(), let stories_copy = try? stories.copy() {
                    let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  indexPath.row)
//                    self.navigationController?.pushViewController(storyPreviewScene, animated: true)
                    self.present(storyPreviewScene, animated: true, completion: nil)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return indexPath.row == 0 ? CGSize(width: 100, height: 100) : CGSize(width: 80, height: 100)
    }
}

//MARK:- TableViewDelegate
extension PulseStoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FirstTableCell = tableView.dequeueReusableCell(withIdentifier: "FirstTableCell") as! FirstTableCell
        cell.ProfileIMG.image = UIImage.init(named: "Avatar5.png")
        cell.titlelbl.text = "Christina Kennedy"
        cell.detaillbl.text = "2 hours ago"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

