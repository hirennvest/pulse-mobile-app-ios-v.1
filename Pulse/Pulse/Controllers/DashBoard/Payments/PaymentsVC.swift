//
//  PaymentsVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-18.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PaymentsVC: BaseVC {
    
    //    MARK:- IBOutlets
    
    @IBOutlet weak var MainSubview: CustomView!
    @IBOutlet var BGLBL: [UILabel]!
    @IBOutlet weak var View_Height: NSLayoutConstraint!
    
    @IBOutlet var DrawerBTN: [CustomBTN]!
    
    @IBOutlet weak var PaymentView: UIView!
    @IBOutlet weak var Titlelbl: UILabel!
    
    @IBOutlet weak var PaymentTBL: UITableView!
    
    @IBOutlet weak var SwipeView: UIView!
    @IBOutlet weak var OptionBTN: MMSlidingButton!
    @IBOutlet var Arrow: [UIButton]!
    
    @IBOutlet weak var AnimationView: UIView!
    
    @IBOutlet weak var CenterVIew: UIView!
    @IBOutlet weak var CenterIMG: UIImageView!
    @IBOutlet weak var Count: UILabel!
    @IBOutlet weak var StatusLBL: UILabel!
    
    //    MARK:- Variable Define
    
    @objc public var DismissCallBack: (() -> Void)?
    var selectedPay: NSMutableArray!
    
    var timer: Timer?
    var progressValue: Int = 0 {
        didSet {
            if progressValue >= 100 {
                self.timer?.invalidate()
                self.timer = nil
            }
        }
    }
    
    let gradientOne = UIColor(red:0.09, green:0.42, blue:0.71, alpha:0.20).cgColor
    let gradientTwo = UIColor(red:0.09, green:0.42, blue:0.71, alpha:0.60).cgColor
    let gradientThree = UIColor(red:0.09, green:0.42, blue:0.71, alpha:1).cgColor
    
    //    MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.selectedPay = NSMutableArray.init()
        
        self.View_Height.constant = Screen_height / 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.PaymentShow()
    }
    
    //    MARK:- User Define methods
    
    func PaymentShow() {
        
        self.PaymentView.isHidden = false
        self.AnimationView.isHidden = true
        
        self.Titlelbl.text = "PULSE PAY".customStringFormatting()
        self.OptionBTN.buttonText = "SLIDE TO PAY".customStringFormatting()
        self.OptionBTN.delegate = self
        self.Count.text = "0%"
        
        self.PaymentTBL.register(UINib.init(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        self.PaymentTBL.tableFooterView = footer
        
        self.PaymentTBL.delegate = self
        self.PaymentTBL.dataSource = self
        self.PaymentTBL.reloadData()
    }
    
    func ShowAnimation() {
        self.PaymentView.isHidden = true
        self.AnimationView.isHidden = false
        self.Count.isHidden = false
        
        self.StatusLBL.text = "PROCESSING".customStringFormatting()
        self.StatusLBL.textColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
        self.CenterIMG.image = UIImage.init(named: "Oval")
        self.CenterIMG.tintColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.CenterIMG.transform = self.CenterIMG.transform.rotated(by: .pi / 2)
        }) { (finished) -> Void in
            if self.progressValue >= 100 {
                self.timer?.invalidate()
                self.timer = nil
                self.CompleteAnimation()
                self.CenterIMG.transform = .identity
            }
            else {
                self.ShowAnimation()
            }
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(progress), userInfo: nil, repeats: true)
    }
    
    @objc func progress() {
        if self.progressValue >= 100 {
            self.timer?.invalidate()
            self.timer = nil
            let randomFunc: [()] = [self.CompleteAnimation(), self.FailedAnimation()]
            let randomResult = Int(arc4random_uniform(UInt32(randomFunc.count)))
            return randomFunc[randomResult]
        }
        else {
            self.progressValue = self.progressValue + 10
            self.Count.text = "\(self.progressValue)%"
        }
    }
    
    func CompleteAnimation() {
        self.PaymentView.isHidden = true
        self.AnimationView.isHidden = false
        self.Count.isHidden = true
        
        self.CenterIMG.transform = .identity
        self.StatusLBL.text = "COMPLETE".customStringFormatting()
        self.StatusLBL.textColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
        self.CenterIMG.image = UIImage.init(named: "SF_checkmark_circle")
        self.CenterIMG.tintColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.DrawerBTN(self.DrawerBTN as Any)
        }
    }
    
    func FailedAnimation() {
        self.PaymentView.isHidden = true
        self.AnimationView.isHidden = false
        self.Count.isHidden = true
        
        self.CenterIMG.transform = .identity
        self.StatusLBL.text = "FAILED".customStringFormatting()
        self.StatusLBL.textColor = UIColor.red
        self.CenterIMG.image = UIImage.init(named: "SF_multiply_circle")
        self.CenterIMG.tintColor = UIColor.red
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.DrawerBTN(self.DrawerBTN as Any)
        }
    }
    
    //    MARK:- API Calling Methods
    
    //    MARK:- IBAction Methods
    
    @IBAction func DrawerBTN(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.DismissCallBack != nil {
                self.DismissCallBack!()
            }
        }
    }
    
}

//MARK:- Slide Delegate
extension PaymentsVC: SlideButtonDelegate {
    
    func buttonStatus(status: SlideStatus, sender: MMSlidingButton) {
        if status == SlideStatus.Complete {
            self.ShowAnimation()
        }
        else {
            self.PaymentView.isHidden = false
            self.AnimationView.isHidden = true
            sender.reset()
        }
    }
    
}

//MARK:- TableViewDelegate
extension PaymentsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PaymentCell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell") as! PaymentCell
        cell.CheckCallBack = {
            self.selectedPay.removeAllObjects()
            self.selectedPay.add(indexPath)
            self.PaymentTBL.reloadData()
        }
        if self.selectedPay.count == 0 {
            self.selectedPay.removeAllObjects()
            self.selectedPay.add(indexPath)
            self.PaymentTBL.reloadData()
        }
        
        cell.IMG.image = indexPath.row == 0 ? UIImage.init(named: "Pulse_Logo") : indexPath.row == 1 ? UIImage.init(named: "visa") : UIImage.init(named: "GXBLUE")
        cell.TitileLBL.text = indexPath.row == 0 ? "Pulse Wallet" : indexPath.row == 1 ? "Visa Ending In 1234" : "VaultConnect"
        self.PaymentView.isHidden = self.selectedPay.count == 0 ? true : false
        if self.selectedPay.contains(indexPath) {
            cell.CheckBTN.isSelected = true
        }
        else {
            cell.CheckBTN.isSelected = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60 //UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedPay.removeAllObjects()
        self.selectedPay.add(indexPath)
        self.PaymentTBL.reloadData()
    }
    
}
