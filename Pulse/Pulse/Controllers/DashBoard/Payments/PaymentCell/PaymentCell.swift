//
//  PaymentCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-19.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {

    @IBOutlet weak var IMG: UIImageView!
    @IBOutlet weak var TitileLBL: UILabel!
    @IBOutlet weak var CheckBTN: UIButton!
    
    @objc public var CheckCallBack: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func CheckBTN(_ sender: Any) {
        if  CheckCallBack != nil {
            self.CheckCallBack!()
        }
    }
    
}
