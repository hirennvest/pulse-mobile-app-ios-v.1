//
//  SubscriptionHeadCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-06.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SubscriptionHeadCell: UICollectionViewCell {

    @IBOutlet weak var RoundView: RoundedCornersView!
    @IBOutlet weak var SubscriptionIMG: UIImageView!
    
    //MARK: - Public iVars
    public var story: IGStory? {
        didSet {
            self.profileNameLabel.text = story?.user.name
            self.profileNameLabel.textColor = .white
            if let picture = story?.user.picture {
                self.SubscriptionIMG.setImage(url: picture, style: .squared, completion: nil)
            }
        }
    }
    public var userDetails: (String,String)? {
        didSet {
            if let details = userDetails {
                self.profileNameLabel.text = details.0
                self.SubscriptionIMG.setImage(url: details.1, style: .squared, completion: nil)
            }
        }
    }
    
    //MARK: -  Private ivars
    private let profileImageView: IGRoundedView = {
        let roundedView = IGRoundedView()
        roundedView.translatesAutoresizingMaskIntoConstraints = false
        return roundedView
    }()
    
    private let profileNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = 10
    }

}
