//
//  NewSubscriptionCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-09.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class NewSubscriptionCell: UITableViewCell {

    @IBOutlet weak var Subview: CustomView!
    @IBOutlet weak var profileIMG: CustomIMG!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var subdetaillbl: UILabel!
    
    //MARK: - Public iVars
    public var story: IGStory? {
        didSet {
            self.namelbl.text = story?.user.name
            if let picture = story?.user.picture {
                self.profileIMG.setImage(url: picture, style: .rounded, completion: nil)
            }
        }
    }
    public var userDetails: (String,String)? {
        didSet {
            if let details = userDetails {
                self.namelbl.text = details.0
                self.profileIMG.setImage(url: details.1, style: .rounded, completion: nil)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
