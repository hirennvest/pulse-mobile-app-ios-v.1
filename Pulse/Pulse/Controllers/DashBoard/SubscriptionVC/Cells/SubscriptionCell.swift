//
//  SubscriptionCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-04.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {

    @IBOutlet weak var Subview: RoundedCornersView!
    @IBOutlet weak var mainimage: UIImageView!
    @IBOutlet weak var Subview_Trailing: NSLayoutConstraint!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var subdetaillbl: UILabel!
    @IBOutlet weak var eyeimage: UIImageView!
    @IBOutlet weak var lightimage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.Subview_Trailing.constant = 5.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
