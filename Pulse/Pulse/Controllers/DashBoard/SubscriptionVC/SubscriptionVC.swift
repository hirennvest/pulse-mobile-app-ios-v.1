//
//  SubscriptionVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-02.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class SubscriptionVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var NavBG: UIView!
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var ProfileBTN: CustomBTN!
    @IBOutlet weak var SearchView: CustomView!
    @IBOutlet weak var SearchBar: UITextField!
    @IBOutlet weak var SettingBTN: CustomBTN!
    
    @IBOutlet weak var SubscribHeaderView: UIView!
    @IBOutlet weak var Header_Height: NSLayoutConstraint!
    @IBOutlet weak var SubscribHeaderBG: UIImageView!
    @IBOutlet weak var SubscribHeaderLBL: UILabel!
    
    @IBOutlet weak var SubscribHeaderCollecction: UICollectionView!
    @IBOutlet weak var Story_Height: NSLayoutConstraint!
    
    @IBOutlet weak var LeftTitleLBL: UILabel!
    @IBOutlet weak var RightTitleLBL: UIButton!
    @IBOutlet weak var TableList: UITableView!
    @IBOutlet weak var TBL_Height: NSLayoutConstraint!
    
    //    MARK:- Variable Define
    
    private var viewModel: IGHomeViewModel = IGHomeViewModel()
    
    lazy var cellWidth:CGFloat = SubscribHeaderCollecction.frame.size.width / 4
    lazy var cellHeight:CGFloat = SubscribHeaderCollecction.frame.size.height
    
    lazy var layout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: self.cellWidth, height: self.cellHeight)
        return flowLayout
    }()
    
    //    MARK:- View Life Cycle
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.Story_Height.constant = Screen_height * 0.1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupview()
    }
    
    //    MARK:- User Define methods
    
    func setupview() {
        
        self.NavBG.backgroundColor = .white
        self.SearchBar.attributedPlaceholder =
            NSAttributedString(string: "Search Anything in Pulse", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        let toolbar = UIToolbar.init(frame: CGRect(origin: .zero, size: .init(width: view.frame.width, height: 30)))
        let flexpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBTN = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(DoneBTNAction))
        toolbar.setItems([flexpace, doneBTN], animated: true)
        toolbar.sizeToFit()
        
        self.SearchBar.inputAccessoryView = toolbar
        
        //        TODO: HEADER COLLECTION
        //        self.SubscribHeaderLBL.text = "SUBSRIPTIONS".customStringFormatting()
        self.SubscribHeaderBG.clipsToBounds = true
        
        self.SubscribHeaderCollecction.register(UINib.init(nibName: "SubscriptionHeadCell", bundle: nil), forCellWithReuseIdentifier: "SubscriptionHeadCell")
        self.SubscribHeaderCollecction.register(UINib.init(nibName: "AddSubcriptionCell", bundle: nil), forCellWithReuseIdentifier: "AddSubcriptionCell")
        self.SubscribHeaderCollecction.translatesAutoresizingMaskIntoConstraints = false
        
        self.SubscribHeaderCollecction.delegate = self
        self.SubscribHeaderCollecction.dataSource = self
        
        //        TODO: Table view define
        
        //        self.LeftTitleLBL.text = "DIRECTPULSE".customStringFormatting()
        self.RightTitleLBL.setTitle("FREE".customStringFormatting(), for: .normal)
        
        self.TableList.register(UINib.init(nibName: "NewSubscriptionCell", bundle: nil), forCellReuseIdentifier: "NewSubscriptionCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        self.TableList.tableFooterView = footer
        
        self.TBL_Height.constant = CGFloat(self.viewModel.getStories()!.count * 69)
        
        self.TableList.delegate = self
        self.TableList.dataSource = self
        self.TableList.reloadData()
        
    }
    
    //    MARK:- Api Calling
    
    func SearchinPulse(searchSTR: String) {
        
    }
    
    //    MARK:- IBAction Methods
    
    @objc func DoneBTNAction () {
        self.view.endEditing(true)
        self.SearchBar.resignFirstResponder()
    }
    
    @IBAction func TappedProfileBTN(_ sender: Any) {
        
    }
    
    @IBAction func TappedSearchBTN(_ sender: Any) {
        if self.SearchBar.text!.count > 0 {
            self.SearchinPulse(searchSTR: self.SearchBar.text!)
        }
    }
    
    @IBAction func TappedSettingBTN(_ sender: Any) {
        
    }
    
    @IBAction func RightTitleTapped(_ sender: Any) {
        UIView.transition(with: self.TableList,
                          duration: 0.35,
                          options: .transitionCurlDown,
                          animations:
            { () -> Void in
                self.TableList.reloadData()
        },completion: nil);
    }
    
    
}

//MARK: - Extension|UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension SubscriptionVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section, Addstory: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 && viewModel.AddStoryBTN {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddSubcriptionCell",for: indexPath) as? AddSubcriptionCell else { fatalError() }
            cell.contentView.clipsToBounds = true
            cell.RoundView.backgroundColor = .white
            cell.SubscriptionIMG.image = UIImage.init(named: "Pulse_Logo")
            cell.SubscriptionIMG.contentMode = .scaleAspectFit
            return cell
        }else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscriptionHeadCell",for: indexPath) as? SubscriptionHeadCell else { fatalError() }
            cell.contentView.clipsToBounds = true
            let story = viewModel.cellForItemAt(indexPath: indexPath)
            cell.story = story
            cell.SubscriptionIMG.contentMode = .scaleToFill
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && viewModel.AddStoryBTN {
            AlertView.showAlertMessage(withMsg: "Try to implement your own functionality for 'Your story'".localized(), withcontroller: self)
        }else {
            DispatchQueue.main.async {
                if let stories = self.viewModel.getStories(), let stories_copy = try? stories.copy() {
                    let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  0)
                    storyPreviewScene.DisplayDetails = true
                    self.present(storyPreviewScene, animated: true, completion: nil)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.cellWidth, height: self.cellHeight)
    }
}

//MARK:- TableViewDelegate
extension SubscriptionVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section, Addstory: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NewSubscriptionCell = tableView.dequeueReusableCell(withIdentifier: "NewSubscriptionCell") as! NewSubscriptionCell
        let story = viewModel.cellForItemAt(indexPath: indexPath)
        cell.story = story
        cell.subdetaillbl.text = "31 GXT"
        //        if indexPath.row == 0 {
        //            cell.Subview_Trailing.constant = 5.0
        //        }
        //        else if indexPath.row == 1 {
        //            cell.Subview_Trailing.constant = 30.0
        //        }
        //        else if indexPath.row == 2 {
        //            cell.Subview_Trailing.constant = 50.0
        //        }
        //        else if indexPath.row == 3 {
        //            cell.Subview_Trailing.constant = 80.0
        //        }
        //        else if indexPath.row == 4 {
        //            cell.Subview_Trailing.constant = 5.0
        //        }
        //        else if indexPath.row == 5 {
        //            cell.Subview_Trailing.constant = 100.0
        //        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let stories = self.viewModel.getStories(), let stories_copy = try? stories.copy() {
                let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  0)
                storyPreviewScene.DisplayDetails = true
                self.present(storyPreviewScene, animated: true, completion: nil)
            }
        }
    }
    
}

//MARK:- Search Bar
extension SubscriptionVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        self.SearchinPulse(searchSTR: newString as String)
        return true
    }
    
}
