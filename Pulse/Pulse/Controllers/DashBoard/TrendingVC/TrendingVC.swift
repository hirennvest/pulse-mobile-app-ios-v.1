//
//  TrendingVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-02.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TrendingVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var Scrollview: UIScrollView!
    
    @IBOutlet weak var TrendHeaderView: UIView!
    @IBOutlet weak var Header_Height: NSLayoutConstraint!
    @IBOutlet weak var TrendHeaderBG: UIImageView!
    @IBOutlet weak var TrendHeaderLBL: UILabel!
    
    @IBOutlet weak var TrendHeaderCollecction: UICollectionView!
    
    @IBOutlet weak var ForuView: UIView!
    @IBOutlet weak var ForU_Height: NSLayoutConstraint!
    @IBOutlet weak var ForuHeaderLBL: UILabel!
    
    @IBOutlet weak var ForuCollecction: UICollectionView!
    
    @IBOutlet weak var LiveLBL: UILabel!
    @IBOutlet weak var LiveTBL: UITableView!
    @IBOutlet weak var Live_Height: NSLayoutConstraint!
    
    //    MARK:- Variable Define
    
    lazy var cellWidth:CGFloat = self.TrendHeaderCollecction.frame.size.width / 2.5
    lazy var cellHeight:CGFloat = self.TrendHeaderCollecction.frame.height
    
    lazy var layout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: self.cellWidth, height: self.cellHeight)
        return flowLayout
    }()
    
    //    MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.Header_Height.constant = Screen_height * 0.3
        self.ForU_Height.constant = Screen_height * 0.2
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupview()
    }
    
    //    MARK:- User Define methods
    
    func setupview() {
        
        //        TODO: HEADER COLLECTION
        self.TrendHeaderLBL.text = "TRENDING".customStringFormatting()
        self.TrendHeaderView.clipsToBounds = true
        self.TrendHeaderBG.clipsToBounds = true
        
        self.TrendHeaderCollecction.register(UINib.init(nibName: "TrendCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TrendCollectionCell")
        self.TrendHeaderCollecction.translatesAutoresizingMaskIntoConstraints = false
        
        self.TrendHeaderCollecction.delegate = self
        self.TrendHeaderCollecction.dataSource = self
        
        //        TODO:- FOR YOU COLLECTION
        self.ForuHeaderLBL.text = "FOR YOU".customStringFormatting()
        
        self.ForuCollecction.register(UINib.init(nibName: "ForyouCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ForyouCollectionViewCell")
        self.ForuCollecction.translatesAutoresizingMaskIntoConstraints = false
        
        self.ForuCollecction.delegate = self
        self.ForuCollecction.dataSource = self
        
        //        TODO: Live Table
        
        self.LiveLBL.text = "LIVE".customStringFormatting()
        self.LiveTBL.register(UINib.init(nibName: "TrendLiveCell", bundle: nil), forCellReuseIdentifier: "TrendLiveCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        self.LiveTBL.tableFooterView = footer
        
        self.Live_Height.constant = 3 * 80
        
        self.LiveTBL.delegate = self
        self.LiveTBL.dataSource = self
        self.LiveTBL.reloadData()
        
        self.Scrollview.contentSize = CGSize(width: Screen_width, height: (self.LiveTBL.frame.origin.y + self.Live_Height.constant + 30))
        
    }
    
}

//MARK: - Extension|UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
extension TrendingVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.TrendHeaderCollecction {
            return 5
        }
        else {
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.TrendHeaderCollecction {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendCollectionCell",for: indexPath) as? TrendCollectionCell else { fatalError() }
            cell.TrendImage.image = UIImage.init(named: "TrendImage.png")
            return cell
        }
        else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForyouCollectionViewCell",for: indexPath) as? ForyouCollectionViewCell else { fatalError() }
            cell.Uimage.image = UIImage.init(named: "TrendImage.png")
            cell.descriptionLabel.text = ""
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.TrendHeaderCollecction {
            DispatchQueue.main.async {
                if let stories = IGHomeViewModel().getStories(), let stories_copy = try? stories.copy() {
                    let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  0)
                    storyPreviewScene.DisplayDetails = true
                    self.present(storyPreviewScene, animated: true, completion: nil)
                }
            }
            //        let vc = PreviewVC.init(nibName: "PreviewVC", bundle: nil)
            //        self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            DispatchQueue.main.async {
                if let stories = IGHomeViewModel().getStories(), let stories_copy = try? stories.copy() {
                    let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  0)
                    storyPreviewScene.DisplayDetails = true
                    self.present(storyPreviewScene, animated: true, completion: nil)
                }
            }
            //        let vc = PreviewVC.init(nibName: "PreviewVC", bundle: nil)
            //        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.TrendHeaderCollecction {
            return CGSize(width: self.cellWidth, height: self.cellHeight)
        }
        else {
            return CGSize(width: self.ForuCollecction.frame.height, height: self.ForuCollecction.frame.height)
        }
    }
    
}

//MARK:- TableViewDelegate
extension TrendingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TrendLiveCell = tableView.dequeueReusableCell(withIdentifier: "TrendLiveCell") as! TrendLiveCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if let stories = IGHomeViewModel().getStories(), let stories_copy = try? stories.copy() {
                let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  0)
                storyPreviewScene.DisplayDetails = true
                self.present(storyPreviewScene, animated: true, completion: nil)
            }
        }
        //        let vc = PreviewVC.init(nibName: "PreviewVC", bundle: nil)
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
