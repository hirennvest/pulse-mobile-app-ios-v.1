//
//  TrendCollectionCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-04.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class TrendCollectionCell: UICollectionViewCell {

    @IBOutlet var TrendImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        TrendImage.clipsToBounds = true
        TrendImage.layer.cornerRadius = 10
    }

}
