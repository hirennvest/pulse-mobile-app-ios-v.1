//
//  ForyouCollectionViewCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-04.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class ForyouCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var Uimage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Uimage.clipsToBounds = true
        Uimage.layer.cornerRadius = 10
    }
    
}
