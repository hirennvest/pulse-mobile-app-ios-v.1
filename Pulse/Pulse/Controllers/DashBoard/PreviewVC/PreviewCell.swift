//
//  PreviewCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class PreviewCell: UICollectionViewCell {

    @IBOutlet weak var PreviewImage: UIImageView!
    @IBOutlet weak var PreviewVideoView: UIView!
    @IBOutlet weak var BackgroundLayer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
