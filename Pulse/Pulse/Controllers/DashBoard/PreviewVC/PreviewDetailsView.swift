//
//  PreviewDetailsView.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-07.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import HCSStarRatingView

public protocol PreviewDetailsViewDelegate {
    func Nextcallback()
    func Previouscallback()
    func Previewcallback()
    func Watchcallback()
    func Savecallback(_ Savedata: NSMutableDictionary)
}

class PreviewDetailsView: UIView {
    
    @IBOutlet weak var PreviewDetailsView: UIView!
    @IBOutlet weak var DetailView: UIView!
    @IBOutlet weak var PreviousBTN: UIButton!
    @IBOutlet weak var DetailTitle: UILabel!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var DetailTime: UILabel!
    @IBOutlet weak var DetailStatus: UILabel!
    @IBOutlet weak var DetailRatingView: HCSStarRatingView!
    @IBOutlet weak var PreviewBTN: RoundedCornersButton!
    @IBOutlet weak var WatchBTN: RoundedCornersButton!
    
    var delegate: PreviewDetailsViewDelegate?
    @objc public var Nextcallback: (() -> Void)?
    @objc public var Previouscallback: (() -> Void)?
    @objc public var Previewcallback: (() -> Void)?
    @objc public var Watchcallback: (() -> Void)?
    @objc public var Savecallback: ((_ Savedata: NSMutableDictionary) -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "PreviewDetailsView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        self.setupDetail()
    }
    
    func setupDetail() {
        self.DetailTitle.text = "DANCE".customStringFormatting()
        self.DetailTime.text = "1:34 Minutes"
        self.DetailRatingView.value = 3.4
        self.DetailStatus.text = "FREE".customStringFormatting()        
    }
    
    @IBAction func NextTapped(_ sender: Any) {
        //        if self.Nextcallback != nil {
        //            self.Nextcallback!(self.ReturnParam)
        //        }
        if self.Nextcallback != nil {
            self.Nextcallback!()
            return
        }
        if self.delegate != nil {
            self.delegate?.Nextcallback()
            return
        }
    }
    
    @IBAction func PreviousTapped(_ sender: Any) {
        if self.Previouscallback != nil {
            self.Previouscallback!()
            return
        }
        if self.delegate != nil {
            self.delegate?.Previouscallback()
            return
        }
    }
    
    @IBAction func PreviewTapped(_ sender: Any) {
        if self.Previewcallback != nil {
            self.Previewcallback!()
            return
        }
        if self.delegate != nil {
            self.delegate?.Previewcallback()
            return
        }
    }
    
    @IBAction func WatchTapped(_ sender: Any) {
        if self.Watchcallback != nil {
            self.Watchcallback!()
            return
        }
        if self.delegate != nil {
            self.delegate?.Watchcallback()
            return
        }
    }
    
}
