//
//  PreviewVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-04.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import HCSStarRatingView
import AVKit
import AVFoundation
import SDWebImage

class PreviewVC: BaseVC {

//    MARK:- IBOutlet Define
    @IBOutlet weak var PreviewCollection: UICollectionView!
    @IBOutlet weak var BackgroundLayer: UIView!
    
    @IBOutlet weak var DetailView: UIView!
    @IBOutlet weak var PreviousBTN: UIButton!
    @IBOutlet weak var DetailTitle: UILabel!
    @IBOutlet weak var NextBTN: UIButton!
    @IBOutlet weak var DetailTime: UILabel!
    @IBOutlet weak var DetailStatus: UILabel!
    @IBOutlet weak var DetailRatingView: HCSStarRatingView!
    @IBOutlet weak var PreviewBTN: RoundedCornersButton!
    @IBOutlet weak var WatchBTN: RoundedCornersButton!
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var AvatarIMG: RoundedCornersImage!
    @IBOutlet weak var AvatarName: UILabel!
    @IBOutlet weak var CloseBTN: UIButton!
    
    
    //    MARK:- Variable Define
    
    lazy var layout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: Screen_width, height: Screen_height)
        return flowLayout
    }()
    
    private var indexOfCellBeforeDragging = 0
    
    private let stories: IGStory? = {
        do {
            return try IGHomeViewModel().getStories()?.stories.first
        }catch let e as MockLoaderError {
            debugPrint(e.description)
        }catch{
            debugPrint("could not read Mock json file :(")
        }
        return nil
    }()
    
//    MARK:- View Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDetail()
    }

//    MARK:- User Define methods
    
    func setupDetail() {
        self.AvatarIMG.image = UIImage.init(named: "")
        self.AvatarName.text = "SARAH".customStringFormatting()
        
        self.PreviewCollection.register(UINib.init(nibName: "PreviewCell", bundle: nil), forCellWithReuseIdentifier: "PreviewCell")
        self.PreviewCollection.translatesAutoresizingMaskIntoConstraints = false
        
        self.PreviewCollection.delegate = self
        self.PreviewCollection.dataSource = self
        
        self.DetailTitle.text = "DANCE".customStringFormatting()
        self.DetailTime.text = "1:34 Minutes"
        self.DetailRatingView.value = 3.4
        self.DetailStatus.text = "FREE".customStringFormatting()
        
    }
    
//    MARK:- IBAction Methods

    @IBAction func CloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func NextTapped(_ sender: Any) {
    }
    
    @IBAction func PreviousTapped(_ sender: Any) {
    }
    
    @IBAction func PreviewTapped(_ sender: Any) {
    }
    
    @IBAction func WatchTapped(_ sender: Any) {
    }
    
}

extension PreviewVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    private func indexOfMajorCell() -> Int {
        let proportionalOffset = self.PreviewCollection.contentOffset.x / Screen_width
        let index = Int(round(proportionalOffset))
        let safeIndex = max(0, min(self.stories!.snaps.count - 1, index))
        return safeIndex
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.stories!.snaps.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviewCell", for: indexPath) as! PreviewCell
        let Snap = self.stories!.snaps[indexPath.row]
        switch Snap.mimeType {
        case MimeType.image.rawValue:
            self.displayImage(cell: cell, indexpath: indexPath)
            break
        case MimeType.video.rawValue:
            self.displayVideo(cell: cell, indexpath: indexPath)
            break
        default:
            break
        }
        
        return cell
    }
    
    func displayImage(cell: PreviewCell, indexpath: IndexPath) {
        let Snap = self.stories!.snaps[indexpath.row]
        cell.PreviewImage.isHidden = false
        cell.PreviewVideoView.isHidden = true
        cell.PreviewImage.sd_setImage(with: URL.init(string: Snap.url), completed: nil)
    }
    
    func displayVideo(cell: PreviewCell, indexpath: IndexPath) {
        let Snap = self.stories!.snaps[indexpath.row]
        cell.PreviewImage.isHidden = true
        cell.PreviewVideoView.isHidden = false
        let videoURL = URL(string: Snap.url)
        let player = AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.backgroundColor = UIColor.black.cgColor
        playerLayer.videoGravity = AVLayerVideoGravity.resize
        playerLayer.frame = cell.PreviewVideoView.bounds
        cell.PreviewVideoView.layer.addSublayer(playerLayer)
        player.play()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Stop scrollView sliding:
        targetContentOffset.pointee = scrollView.contentOffset
        
        // calculate where scrollView should snap to:
        let indexOfMajorCell = self.indexOfMajorCell()
        
        // calculate conditions:
        let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
        let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < self.stories!.snaps.count && velocity.x > swipeVelocityThreshold
        let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
        let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
        let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
        
        if didUseSwipeToSkipCell {
            
            let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
            let toValue = layout.itemSize.width * CGFloat(snapToIndex)
            
            // Damping equal 1 => no oscillations => decay animation:
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
                scrollView.contentOffset = CGPoint(x: toValue, y: 0)
                scrollView.layoutIfNeeded()
            }, completion: nil)
            
        } else {
            // This is a much better way to scroll to a cell:
            let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
            self.PreviewCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
    }
}
