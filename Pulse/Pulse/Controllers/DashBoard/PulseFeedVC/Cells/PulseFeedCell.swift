//
//  PulseFeedCell.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-13.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import SwipeCellKit

class PulseFeedCell: SwipeTableViewCell {

    @IBOutlet weak var FeedCell: CustomView!
    @IBOutlet weak var SubView: CustomView!
    @IBOutlet weak var ColorLBL: UILabel!
    @IBOutlet weak var CellMSG: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let maskPath = UIBezierPath(roundedRect: CGRect.init(x: 0, y: 0, width: self.ColorLBL.frame.width, height: self.ColorLBL.frame.height),
                                    byRoundingCorners: [.bottomLeft, .topLeft],
                                    cornerRadii: CGSize(width: 50.0, height: 50.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        self.ColorLBL.layer.mask = maskLayer
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
