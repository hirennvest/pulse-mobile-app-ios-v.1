//
//  PulseFeedVC.swift
//  Pulse
//
//  Created by Shorupan Pirakaspathy on 2020-04-02.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import SwipeCellKit
import AudioToolbox.AudioServices
import SkeletonView


class PulseFeedVC: BaseVC {
    
    //    MARK:- IBOutlet
    @IBOutlet weak var TopBG: UIView!
    
    @IBOutlet weak var FreeBTN: UIButton!
    @IBOutlet weak var PremiumBTN: UIButton!
    @IBOutlet weak var PrivateBTN: UIButton!
    
    @IBOutlet weak var TableList: UITableView!
    
    //    MARK:- Variable Define
    
    var SelectedTag: Int = 1
    var Freefeed_Arry: NSMutableArray!
    var Primefeed_Arry: NSMutableArray!
    var Privatefeed_Arry: NSMutableArray!
    
    lazy var NoMessage: UILabel = {
        () -> UILabel in
        let lbl = UILabel.init(frame: CGRect(x: 20, y: (Screen_height / 2) - 20, width: Screen_width - 40, height: 20))
        lbl.text = "No More Messages"
        lbl.textColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
        lbl.textAlignment = .center
        lbl.isHidden = true
        self.view.addSubview(lbl)
        return lbl
    }()
    
    //    MARK:- View Life Cycle
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupview()
    }
    
    //    MARK:- User Define methods
    
    func setupview() {
        
        self.Freefeed_Arry = NSMutableArray.init()
        self.Primefeed_Arry = NSMutableArray.init()
        self.Privatefeed_Arry = NSMutableArray.init()
        
        for _ in 1...6 {
            self.Freefeed_Arry.add("add")
            self.Primefeed_Arry.add("add")
            self.Privatefeed_Arry.add("add")
        }
        
        self.selectedOption(tag: 1)
        self.TableList.register(UINib.init(nibName: "PulseFeedCell", bundle: nil), forCellReuseIdentifier: "PulseFeedCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: Screen_width, height: 30))
        footer.backgroundColor = .white
        self.TableList.tableFooterView = footer
        
        self.TableList.delegate = self
        self.TableList.dataSource = self
        self.TableList.reloadData()
        
        self.TableList.isSkeletonable = true
        view.showAnimatedSkeleton()
        
        let gradient = SkeletonGradient(baseColor: SkeletonAppearance.default.tintColor)
        view.updateAnimatedGradientSkeleton(usingGradient: gradient)
        
    }
    
    func selectedOption(tag: Int) {
        self.SelectedTag = tag
        switch tag {
        case 0:
            self.FreeBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
            self.PremiumBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 17)
            self.PrivateBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 17)
            break
        case 1:
            self.FreeBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 17)
            self.PremiumBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
            self.PrivateBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 17)
            break
        case 2:
            self.FreeBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 17)
            self.PremiumBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 17)
            self.PrivateBTN.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
            break
        default:
            break
        }
        UIView.transition(with: self.TableList,
                          duration: 0.35,
                          options: .transitionCurlDown,
                          animations:
            { () -> Void in
                self.CheckDataStatus()
        },completion: nil);
    }
    
    func configure(action: SwipeAction, with descriptor: ActionDescriptor) {
        action.title = descriptor.title(forDisplayMode: .imageOnly)
        action.image = descriptor.image(forStyle: .backgroundColor, displayMode: .imageOnly)
        action.backgroundColor = descriptor.color(forStyle: .backgroundColor)
        action.transitionDelegate = self
    }
    
    func CheckDataStatus() {
        DispatchQueue.main.async {
            if self.SelectedTag == 0 {
                if self.Freefeed_Arry.count == 0 {
                    self.NoMessage.isHidden = false
                    self.TableList.isHidden = true
                }
                else {
                    self.NoMessage.isHidden = true
                    self.TableList.isHidden = false
                    self.TableList.reloadData()
                }
            }
            else if self.SelectedTag == 1 {
                if self.Primefeed_Arry.count == 0 {
                    self.NoMessage.isHidden = false
                    self.TableList.isHidden = true
                }
                else {
                    self.NoMessage.isHidden = true
                    self.TableList.isHidden = false
                    self.TableList.reloadData()
                }
            }
            else {
                if self.Privatefeed_Arry.count == 0 {
                    self.NoMessage.isHidden = false
                    self.TableList.isHidden = true
                }
                else {
                    self.NoMessage.isHidden = true
                    self.TableList.isHidden = false
                    self.TableList.reloadData()
                }
            }
        }
    }
    
    //    MARK:- Api Calling
    
    func SearchinPulse(Key: PlistKey) {
        LottieHUD.shared.showHUD()
        NetworkingRequests.shared.NetworkPostrequestJSON(Key, Parameters: [:], Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
            if statuscode {
                LottieHUD.shared.stopHUD()
            }
            else {
                LottieHUD.shared.stopHUD()
            }
        }) { (msg, code) in
            LottieHUD.shared.stopHUD()
        }
    }
    
    //    MARK:- IBAction Methods
    
    @IBAction func TappedFreeBTN(_ sender: Any) {
        self.selectedOption(tag: 0)
    }
    
    @IBAction func TappedPremiumBTN(_ sender: Any) {
        self.selectedOption(tag: 1)
    }
    
    @IBAction func TappedPrivateBTN(_ sender: Any) {
        self.selectedOption(tag: 2)
    }
    
}

//MARK:- TableViewDelegate
extension PulseFeedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.SelectedTag == 0 {
            return self.Freefeed_Arry.count
        }
        else if self.SelectedTag == 1 {
            return self.Primefeed_Arry.count
        }
        else {
            return self.Privatefeed_Arry.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PulseFeedCell = tableView.dequeueReusableCell(withIdentifier: "PulseFeedCell") as! PulseFeedCell
        cell.delegate = self
        cell.ColorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
        cell.CellMSG.text = ""
        cell.SubView.isHidden = false
        if self.SelectedTag == 0 {
//            let lbl = self.Freefeed_Arry.object(at: indexPath.row)
        }
        else if self.SelectedTag == 1 {
//            let lbl = self.Primefeed_Arry.object(at: indexPath.row)
        }
        else {
//            let lbl = self.Privatefeed_Arry.object(at: indexPath.row)
        }
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        longPressRecognizer.minimumPressDuration = 0.8
        longPressRecognizer.allowableMovement = 15
        cell.addGestureRecognizer(longPressRecognizer)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        DispatchQueue.main.async {
//            let pay = PaymentsVC.init(nibName: "PaymentsVC", bundle: nil)
//            pay.modalPresentationStyle = .overCurrentContext
//            pay.modalTransitionStyle = .coverVertical
//            pay.DismissCallBack = {
//
//            }
//            self.present(pay, animated: true) {
//
//            }
//        }
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer)
    {
        if sender.state == .began {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            sleep(1)
            let touchPoint = sender.location(in: self.TableList)
            if let indexPath = self.TableList.indexPathForRow(at: touchPoint) {
                let cell = self.TableList.cellForRow(at: indexPath) as! PulseFeedCell
                cell.SubView.isHidden = true
                cell.FeedCell.backgroundColor = UIColor.colorWithHexString(hexStr: "699ECE")
                cell.ColorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: "699ECE")
                cell.FeedCell.BGColor = UIColor.colorWithHexString(hexStr: "699ECE")
                cell.CellMSG.text = "WATCH?".customStringFormatting()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if self.SelectedTag == 0 {
                        let lbl = self.Freefeed_Arry.object(at: indexPath.row)
                        print(lbl)
                    }
                    else if self.SelectedTag == 1 {
                        let lbl = self.Primefeed_Arry.object(at: indexPath.row)
                        print(lbl)
                    }
                    else {
                        let lbl = self.Privatefeed_Arry.object(at: indexPath.row)
                        print(lbl)
                    }
                    if let stories = IGHomeViewModel().getStories(), let stories_copy = try? stories.copy() {
                        let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  0)
                        storyPreviewScene.DisplayDetails = true
                        self.present(storyPreviewScene, animated: true) {
                            cell.FeedCell.backgroundColor = UIColor.white
                            cell.FeedCell.BGColor = UIColor.white
                            cell.ColorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
                            cell.CellMSG.text = "".customStringFormatting()
                            cell.SubView.isHidden = false
                        }
                    }
                }
            }
        }
    }
    
}

extension PulseFeedVC: SwipeTableViewCellDelegate, SwipeActionTransitioning {
    
    func didTransition(with context: SwipeActionTransitioningContext) -> Void {
        if context.newPercentVisible >= 2.5 {
            if #available(iOS 10.0, *) {
                Vibration.light.vibrate()
            }
            else {
                Vibration.oldSchool.vibrate()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let cell = tableView.cellForRow(at: indexPath) as! PulseFeedCell
        if orientation == .left {
            let Save = SwipeAction(style: .default, title: nil) { action, indexPath in
                cell.FeedCell.backgroundColor = UIColor.white
                cell.FeedCell.BGColor = UIColor.white
                cell.ColorLBL.backgroundColor = .white
                cell.SubView.isHidden = true
                cell.FeedCell.clipsToBounds = true
                cell.FeedCell.layer.masksToBounds = true
                cell.CellMSG.text = "SAVED".customStringFormatting()
                cell.CellMSG.textColor = UIColor.colorWithHexString(hexStr: "186AB4")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if self.SelectedTag == 0 {
                        self.Freefeed_Arry.removeObject(at: indexPath.row)
                    }
                    else if self.SelectedTag == 1 {
                        self.Primefeed_Arry.removeObject(at: indexPath.row)
                    }
                    else {
                        self.Privatefeed_Arry.removeObject(at: indexPath.row)
                    }
                    self.TableList.deleteRows(at: [indexPath], with: .automatic)
                    self.CheckDataStatus()
                }
            }
            Save.hidesWhenSelected = true
            configure(action: Save, with: .save)
            return [Save]
        } else {
            let delete = SwipeAction(style: .default, title: nil) { action, indexPath in
                //cell.FeedCell.backgroundColor = UIColor.white
                //cell.FeedCell.BGColor = UIColor.white
                //cell.ColorLBL.backgroundColor = .white
                //cell.SubView.isHidden = true
                //cell.FeedCell.clipsToBounds = true
                //cell.FeedCell.layer.masksToBounds = true
                //cell.CellMSG.text = "DELETED".customStringFormatting()
                //cell.CellMSG.textColor = UIColor.colorWithHexString(hexStr: "186AB4")
                //DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.SelectedTag == 0 {
                    self.Freefeed_Arry.removeObject(at: indexPath.row)
                }
                else if self.SelectedTag == 1 {
                    self.Primefeed_Arry.removeObject(at: indexPath.row)
                }
                else {
                    self.Privatefeed_Arry.removeObject(at: indexPath.row)
                }
                //self.TableList.deleteRows(at: [indexPath], with: .automatic)
                self.CheckDataStatus()
                //}
            }
            delete.hidesWhenSelected = true
            configure(action: delete, with: .trash)
            return [delete]
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = orientation == .left ? .none : .destructive(automaticallyDelete: true) //SwipeExpansionStyle(target: .edgeInset(0), additionalTriggers: [.overscroll(Screen_width - 100)], completionAnimation: .fill(.automatic(.delete, timing: .with)))
        options.transitionStyle = .border
        options.backgroundColor = .clear
        options.buttonSpacing = 0
        return options
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        let cell = tableView.cellForRow(at: indexPath) as! PulseFeedCell
        if orientation == .left {
            cell.SubView.isHidden = true
            cell.ColorLBL.isHidden = true
            cell.CellMSG.text = "SAVE?".customStringFormatting()
            cell.CellMSG.textColor = UIColor.colorWithHexString(hexStr: "699ECE")
        } else {
            cell.FeedCell.backgroundColor = UIColor.colorWithHexString(hexStr: "699ECE")
            cell.FeedCell.BGColor = UIColor.colorWithHexString(hexStr: "699ECE")
            cell.ColorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: "699ECE")
            cell.SubView.isHidden = true
            cell.CellMSG.text = "DELETE?".customStringFormatting()
            cell.CellMSG.textColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        let cell = tableView.cellForRow(at: indexPath!) as! PulseFeedCell
        if orientation == .left {
            cell.FeedCell.backgroundColor = UIColor.white
            cell.FeedCell.BGColor = UIColor.white
            cell.CellMSG.text = "".customStringFormatting()
            cell.SubView.isHidden = false
            cell.ColorLBL.isHidden = false
        } else {
            cell.FeedCell.backgroundColor = UIColor.white
            cell.FeedCell.BGColor = UIColor.white
            cell.ColorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: AppDarkHex)
            cell.CellMSG.text = "".customStringFormatting()
            cell.SubView.isHidden = false
            cell.ColorLBL.isHidden = false
        }
    }
    
}
