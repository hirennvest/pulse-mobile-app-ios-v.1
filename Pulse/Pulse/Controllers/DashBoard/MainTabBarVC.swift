//
//  MainTabBarVC.swift
//  Demo
//
//  Created by Shorupan Pirakaspathy on 2020-04-04.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class MainTabBarVC: UIViewController {
    
//    MARK:- IBOutlet Define
    
    @IBOutlet weak var TabBGview: UIView!
    @IBOutlet weak var TabView: UIView!
    @IBOutlet weak var Tab1BTN: UIButton!
    @IBOutlet weak var Tab2BTN: UIButton!
    @IBOutlet weak var Tab3BTN: UIButton!
    @IBOutlet weak var Tab4BTN: UIButton!
    @IBOutlet weak var Tab5BTN: UIButton!
    
//    MARK:- Variable Define
    
    var containerController: SwiftyPageController!
    
//    MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Tab3BTN.clipsToBounds = true
        self.Tab3BTN.layer.cornerRadius = self.Tab3BTN.frame.height / 2
        self.Tab3BTN.layer.borderWidth = 3
        self.Tab3BTN.layer.borderColor = AppDarkColor.cgColor
    }

//    MARK:- UserDefine Methods
    
    func setupContainerController(_ controller: SwiftyPageController) {
        // assign variable
        containerController = controller
        
        // set delegate
        containerController.delegate = self
        
        // set animation type
        containerController.animator = .default
        
        // set view controllers
        let v1 = PulseFeedVC.init(nibName: "PulseFeedVC", bundle: nil)
        let v2 = SubscriptionVC.init(nibName: "SubscriptionVC", bundle: nil)
        let v3 = TrendingVC.init(nibName: "TrendingVC", bundle: nil)
        let v4 = PulseStoryVC.init(nibName: "PulseStoryVC", bundle: nil)
        let v5 = FifthVC.init(nibName: "FifthVC", bundle: nil)
        
        containerController.viewControllers = [v1, v2, v3, v4, v5]
        
        // select needed controller
        containerController.selectController(atIndex: 0, animated: false)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let containerController = segue.destination as? SwiftyPageController {
            setupContainerController(containerController)
        }
    }
    
//    MARK:- IBAction Methods
    
    @IBAction func Tab1Tapped(_ sender: Any) {
        containerController.selectController(atIndex: 0, animated: true)
    }
    
    @IBAction func Tab2Tapped(_ sender: Any) {
        containerController.selectController(atIndex: 1, animated: true)
    }
    
    @IBAction func Tab3Tapped(_ sender: Any) {
        containerController.selectController(atIndex: 2, animated: true)
    }
    
    @IBAction func Tab4Tapped(_ sender: Any) {
        containerController.selectController(atIndex: 3, animated: true)
    }
    
    @IBAction func Tab5Tapped(_ sender: Any) {
        containerController.selectController(atIndex: 4, animated: true)
    }
    
}

// MARK: - PagesViewControllerDelegate

extension MainTabBarVC: SwiftyPageControllerDelegate {
    
    func swiftyPageController(_ controller: SwiftyPageController, alongSideTransitionToController toController: UIViewController) {
        
    }
    
    func swiftyPageController(_ controller: SwiftyPageController, didMoveToController toController: UIViewController) {
//        segmentControl.selectedSegmentIndex = containerController.viewControllers.index(of: toController)!
    }
    
    func swiftyPageController(_ controller: SwiftyPageController, willMoveToController toController: UIViewController) {
        
    }
    
}
